﻿using UnityEngine;

public class Controller : UnityEngine.MonoBehaviour
{
    // Variables publicas para poder modifircar desde el inspector
    public float maxS = 11f;

    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float jumping = 0f;
    private float move = 0f;
    private bool right = false;
    private Animator anim;

    private bool flipped = false;
    bool attack = false;
    bool dead = false;
    bool jump = false;


    // Use this for initialization
    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();

        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        attack = Input.GetKeyDown(KeyCode.Space);
        jump = Input.GetKeyDown(KeyCode.UpArrow);

        if (attack)
        {
            anim.SetBool("attacking", attack);
        }

        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

        move = Input.GetAxis("Vertical");
        rb2d.velocity = new Vector2(rb2d.velocity.x, move * maxS);

        //Miramos si nos estamos moviendo.
        // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
        if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
        {
            if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
            anim.SetBool("walking", true);
        }
        else
        {
            anim.SetBool("walking", false);
        }

        if (dead)
        {
            Application.LoadLevel("game");
        }


    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "dead")
        {
            dead = true;
            anim.SetBool("dead", dead);
            maxS = 0;
        }

        if (coll.gameObject.tag == "vivaMiPolla")
        {
            Application.LoadLevel("finale");
        }
    }
}
